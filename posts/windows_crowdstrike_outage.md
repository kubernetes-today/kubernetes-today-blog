---
title: Global Disruption, CrowdStrike Update Cripples Windows
date: July 22, 2024
tags: Miscellaneous
author: Jose Angel Morena
---
A recent update from the cybersecurity giant, CrowdStrike, has precipitated a widespread failure across Microsoft's Windows and Azure platforms. The intricate web connecting these systems to the global network has triggered a domino effect, severely disrupting operations for numerous companies. This initial glitch cascaded down to affect their customers and suppliers, primarily impacting corporate networks and cloud-based business functions more than personal devices.

The fallout has been dramatic, with major interruptions at airports, train stations, financial transactions freezing, and healthcare systems struggling to function. This incident underscores the vulnerability of digital infrastructures, leading to a significant digital blackout affecting a vast array of private and public entities worldwide.

CrowdStrike, known for its Falcon Sensor software aimed at thwarting cyber threats, identified a critical bug that led to widespread Windows system crashes, manifested as the dreaded Blue Screen of Death (BSOD).

As of this report, the issue has led to approximately 8.5 million Windows systems worldwide being rendered inoperative, highlighting the extensive reach and impact of this disruption.

<div style="text-align: center;">
    <img src="https://kubernetestoday.tech/images/windows_crowdstrike_issue.webp" alt="CrowdStrike Meme" style="width: 50%; height: auto;">
</div>

# Workaround steps for individual Windows hosts

If you're dealing with a specific system issue, here’s a helpful workaround provided by CrowdStrike:

1. **Boot Windows into Safe Mode or the Windows Recovery Environment.**
2. **Navigate to the `C:\Windows\System32\drivers\CrowdStrike` directory.**
3. **Find and delete the file named `C-00000291*.sys`.**
4. **Restart the computer normally.**

For further instructions, please consult the CrowdStrike blog: [CrowdStrike Blog](https://lnkd.in/gJXrhApC).

# Navigating Setbacks: Enhancing Resilience in Personal and Organizational Growth

A recent update from cybersecurity firm CrowdStrike inadvertently triggered a series of failures across Microsoft’s Windows and Azure platforms. Given the deep interconnections of these systems with the global network, this sparked a cascade effect. Operations across numerous companies were disrupted, causing subsequent issues for their customers and suppliers. These disturbances were especially pronounced within corporate networks and cloud-dependent business activities, as opposed to personal Windows devices.

**“Even industry giants like Microsoft and CrowdStrike aren’t immune to errors.”**

Dwelling on mistakes doesn't benefit anyone. It's a common occurrence, even among the most prominent organizations, to encounter setbacks. The key lies in promptly addressing these errors, developing solutions, and learning from them to foster both personal and organizational growth.

## Leveraging Errors for Improvement

Mistakes should be viewed as precious learning opportunities that are vital for development. It's crucial from an early age to learn to focus on solutions and glean insights from each setback. Furthermore, investing in Quality Assurance (QA) is imperative. Far from being just an additional cost, QA is a fundamental investment in enhancing product quality and customer satisfaction, which reduces the long-term expenses linked to mishaps and boosts brand credibility.

## Reevaluating IT’s Role

Often perceived merely as a cost center, IT is actually pivotal in ensuring the seamless operation of applications and infrastructure. Effective IT teams have preempted issues like those caused by the CrowdStrike update, whether through alternative operating systems like Linux or by maintaining stricter control over updates, thereby safeguarding organizational functions.

## Conclusion: Elevating Standards in Critical Systems

Mistakes are a universal human experience. I make them, you make them—everyone does. Yet, when mishaps occur within critical systems, it's typically a sign of a broader systemic issue.

To those who argue that entities like CrowdStrike and Microsoft are beyond reproach: It's time to elevate our standards. This industry underpins vital infrastructure that impacts hospitals, global transport, and more. Catastrophic failures that disrupt these essential services are unacceptable.

Although no system can be perfect, striving for perfection is essential. The notion that settling for "good enough" to cut costs is pragmatic is not only simplistic but also fundamentally flawed. In fields that support critical infrastructure, "good enough" simply isn't sufficient. We must aim for excellence, continually improving our standards to prevent failures that could have widespread, devastating effects.