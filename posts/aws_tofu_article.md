---
title: Provisioning in AWS using OpenTofu
date: July 26, 2024
tags: Provisioning
author: Jose Angel Morena
---
In today's article, we'll dive deep into Infrastructure as Code and OpenTofu, and demonstrate how to provision infrastructure in AWS using OpenTofu. Happy Hacking!

## What is Infrastructure as Code (IaC)?

Infrastructure as Code (IaC) is a crucial approach in modern IT that involves managing and provisioning infrastructure through machine-readable definition files. This practice allows developers and systems administrators to automatically manage, monitor, and provision resources in cloud environments, rather than manually configuring discrete instances and other resources.

#### Key Properties of IaC:
- **Automation**: IaC automates the provisioning of infrastructure, ensuring that deployments are repeatable and more error-proof by reducing manual interventions..
- **Consistency**: By codifying infrastructure, teams ensure that the setup is consistent across development, testing, and production environments.
- **Speed**: IaC reduces the time required to set up and configure resources, speeding up project timelines.
- **Accountability and Transparency**: With IaC, all changes to the infrastructure are tracked and version-controlled, enhancing accountability and transparency.

#### Benefits of IaC:
- **Single Source of Configuration**: Having a single repository for all infrastructure configurations simplifies management and oversight. It reduces the complexity of handling multiple scripts or manual setups across different environments.
- **Ease of Access and Control**: Centralized management allows for easier updates and changes, with a clear audit trail of who made changes and when. This is particularly beneficial in regulated industries where compliance is critical.
- **Consistent Environments**: The use of version-controlled IaC ensures that all deployments, from development to production, are consistent. This drastically reduces the "works on my machine" syndrome that can plague teams not using IaC.
- **Automated Correction**: Tools like Terraform, OpenTofu, Ansible, and others can automatically detect and correct drift from the desired state, ensuring that the deployed infrastructure matches the specified configurations precisely.
- **Rapid Recovery**: In the event of a failure, the infrastructure can be spun up in a new location or restored in the current location without manual intervention, saving valuable time during critical periods.
- **Testable Recovery Procedures**: With IaC, disaster recovery processes can be tested and verified to ensure they work as expected without impacting the actual production environments. This testing can be automated and performed regularly to ensure readiness.
- **Immutable Infrastructure**: IaC supports the concept of immutable infrastructure, where changes are made by replacing components rather than altering existing ones. This reduces the risk of unauthorized changes and security vulnerabilities.
- **Security as Code**: Security policies and configurations can be integrated directly into IaC scripts, ensuring that security measures are uniformly applied at all times.
- **Scalable Operations**: IaC makes scaling infrastructure to meet increasing demand more manageable and less error-prone, as the same declarative files can be used to provision more resources as needed.
- **Flexibility in Deployment**: Teams can deploy the same setup in different environments or clouds without reconfiguration, providing flexibility and options for operational strategies.

## What is OpenTofu

[OpenTofu](https://opentofu.org/) is a [Terraform](https://www.terraform.io/) fork, created as an initiative of Gruntwork, Spacelift, Harness, Env0, Scalr, and many others, in response to HashiCorp’s switch from an open-source license to the BUSL (Business Source License). Just like Terraform, it is an infrastructure as code tool that lets you build, change, and version cloud and on-prem resources safely and efficiently.

OpenTofu is a cloud-agnostic tool, which means it can manage infrastructure across various platforms and cloud providers. For more details on the supported providers, you can visit the [OpenTofu documentation](https://library.tf/providers).

<div style="text-align: center;">
    <img src="https://kubernetestoday.tech/images/opentofu-logo.png" alt="Tofu" style="width: 50%; height: auto;">
</div>

### What is State in OpenTofu?

The **state** is a crucial concept in OpenTofu. It refers to the persistent data store used by OpenTofu to map real world resources to your configuration, keep track of metadata, and improve performance for large infrastructures.

**State File**: This is a JSON file that OpenTofu generates, which contains all data necessary to map resources to the configuration and keep track of dependencies. By default, this file is named `terraform.tfstate` and is stored locally, but it can be configured to be stored remotely, which is recommended for team environments to avoid conflicts.

#### Purpose of State:
- **Mapping**: The state file maps the resources in your configuration files to the actual resources in the cloud provider.
- **Metadata Storage**: It stores metadata such as resource dependencies which Terraform uses to create and manage resources in a correct order.
- **Performance**: By storing information about your infrastructure, Terraform can make operations like `apply` much quicker, as it doesn’t need to query the cloud provider for current infrastructure status.

**Remote State**: For teams and larger projects, managing state files locally becomes impractical. Terraform supports storing state remotely (e.g. AWS S3, HashiCorp Consul, or [many others](https://opentofu.org/docs/language/settings/backends/configuration/)), which helps with team collaboration and state locking to prevent conflicts during concurrent operations.

## Let's Get Started with Provisioning!

In today’s tutorial we will be provisioning AWS infrastructure using OpenTofu. We'll cover the configuration of a Virtual Private Cloud (VPC), subnets, an internet gateway, a route table, security groups, and multiple AWS instances. Additionally, we'll integrate OpenTofu with Ansible to configure the systems after deployment.

**Step 1: Clone the Repository**
Begin by visiting and cloning our specially prepared repository from [this link](https://gitlab.com/kubernetes-today/aws-terraform-instance.git).

**Step 2: Check Prerequisites**
Ensure you meet [all the prerequisites](https://gitlab.com/kubernetes-today/aws-opentofu-example#prerequisites) before proceeding. You'll need Ansible and OpenTofu installed on your system, along with an active AWS account. This tutorial aims to stay within the limits of the AWS free tier.

**Step 3: AWS Credentials**
Make sure you can create an AWS access key for OpenTofu to use. For instructions, check out [this guide](https://gitlab.com/kubernetes-today/aws-opentofu-example#get-aws-credentials). Set up the necessary environment variables for provisioning:

```bash
export AWS_ACCESS_KEY_ID=<your-access-key>
export AWS_SECRET_ACCESS_KEY=<your-secret-access-key>
```

**Step 4: Set Up SSH Key**
Create an authorized SSH key for the `ec2-user`, as OpenTofu will use this key to manage AWS EC2 Key Pairs. Instructions for setting up the SSH key are available [here](https://gitlab.com/kubernetes-today/aws-opentofu-example#setup-ssh-key-for-ec2-instance).

**Step 5: Begin Provisioning**
Now, with everything set, you're ready to start the provisioning process. Consider editing the `variables.tf` file if you wish to change any setting. In my case I will set `instance_count` to five so I provision five instances.

Now you are ready to run [these commands](https://gitlab.com/kubernetes-today/aws-opentofu-example/-/blob/main/README.md#deploy-infrastructure-with-opentofu) which will provision all resources in your AWS account.

After running `tofu apply -auto-approve` you will be able to see public IP for your instances.

**Step 6: Run Ansible Playbook**

Now that your infrastructure is provisioned using OpenTofu, it's time to configure it with Ansible. In this step, you will see how to effectively integrate Ansible with OpenTofu to manage and configure your systems.

To run the Ansible Playbook, follow the detailed commands provided in [this section of the repository](https://gitlab.com/kubernetes-today/aws-opentofu-example/-/blob/main/README.md#ansible). After executing the playbook on the newly provisioned instances, it will show the operating system details for each system.

The integration between Ansible and OpenTofu is pivotal for automating the configuration process. It leverages the state data from OpenTofu to dynamically generate an inventory that Ansible can use.

- The `ansible.cfg` file is pre-configured to utilize a dynamic inventory script named `ansible-inventory.py`.
- This script executes the `tofu output -json` command to fetch the latest state from OpenTofu and generates an Ansible inventory structure based on this data.

#### Ansible Configuration Details:
- **User and Authentication**: Ansible will operate as the `ec2-user` using the SSH key you previously set up.
- **Inventory Management**: The dynamic inventory plugin, `ansible-inventory.py`, will consult the OpenTofu state to create an accurate and up-to-date inventory for Ansible.

If this step completes successfully, your systems can now be configured consistently. This process highlights the powerful synergy between infrastructure provisioning via OpenTofu and configuration management via Ansible.

#### Step 7: Cleanup Infrastructure
After successfully provisioning and configuring your AWS infrastructure with OpenTofu and Ansible, it’s crucial to know how to properly dismantle and remove all created resources to avoid unnecessary costs or to maintain a clean environment. [This final step](https://gitlab.com/kubernetes-today/aws-opentofu-example/-/blob/main/README.md#cleanup-resources) involves a systematic cleanup of all resources deployed during the tutorial.

## Conclusion

Throughout this article, we've delved into the concept of Infrastructure as Code (IaC) and introduced you to OpenTofu, a robust fork of Terraform developed in response to licensing changes by HashiCorp. By exploring a practical example, you learned how to use OpenTofu to efficiently provision resources on AWS and configure them using Ansible.

We hope this guide has been informative and useful in showcasing how OpenTofu can serve as a powerful tool in your IaC arsenal, facilitating the management of cloud and on-premises resources across various platforms. Whether you're just starting out with IaC or looking to expand your toolkit, OpenTofu offers a flexible, cloud-agnostic solution that aligns with modern DevOps/SRE practices.

If you have ideas for improvements or if you've encountered issues while following the tutorial, your contributions and feedback are highly valued! Feel free to engage with us by creating an issue or submitting a Merge Request (MR) to [our repository](https://gitlab.com/kubernetes-today/aws-opentofu-example).

Thank you for following along, and we look forward to assisting you in your future projects!