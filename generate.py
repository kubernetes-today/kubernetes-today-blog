import os
import math
import shutil
from bs4 import BeautifulSoup
from markdown import markdown
from jinja2 import Environment, FileSystemLoader
from datetime import datetime

# Setup environment paths
POSTS_DIR = 'posts'
OUTPUT_DIR = 'public'
TEMPLATES_DIR = 'templates'
STATIC_DIR = 'static'
ARTICLES_TO_EXCLUDE = [
    'article.contributing.example.md'
]

# Create Jinja environment
env = Environment(loader=FileSystemLoader(TEMPLATES_DIR))
if not os.path.exists(OUTPUT_DIR):
    os.makedirs(OUTPUT_DIR)

def load_posts():
    """Load all markdown posts, extract metadata and content, and sort by date."""
    posts = []
    for filename in filter(lambda f: f.endswith('.md'), os.listdir(POSTS_DIR)):
        if filename not in ARTICLES_TO_EXCLUDE:
            filepath = os.path.join(POSTS_DIR, filename)
            with open(filepath, 'r', encoding='utf-8') as file:
                text = file.read()
            metadata, content = tokenize_text(text)
            posts.append({
                'filename': filename,
                'metadata': metadata,
                'content': content
            })

    # Sort posts by date from metadata
    try:
        posts.sort(key=lambda x: datetime.strptime(x['metadata']['date'], "%B %d, %Y"), reverse=True)
    except KeyError:
        raise ValueError("Missing metadata. Please check out the example post https://gitlab.com/kubernetes-today/kubernetes-today-blog/-/raw/develop/posts/article.contributing.example.md")
    return posts

def nl2br(s):
    return '<br>\n'.join(s.split('\n'))

def add_copy_buttons(html_content):
    """Adds copy buttons to all code blocks in the given HTML content."""
    soup = BeautifulSoup(html_content, 'html.parser')
    for code_block in soup.find_all("div", class_="codehilite"):
        # Ensure the code block container is relatively positioned
        code_block['style'] = 'position: relative; background: #f6f8fa; border: 1px solid #ccc; padding: 10px;'

        # Create the copy button with inline styles for absolute positioning
        copy_button = soup.new_tag(
            "button",
            **{
                'class': 'copy-btn',
                'onclick': 'copyToClipboard(this)',
            }
        )
        copy_button.string = "Copy"
        code_block.insert(0, copy_button)
    return str(soup)

def tokenize_text(text):
    """Parse markdown text to extract front matter and content and add copy buttons to code blocks."""
    if '---' not in text[:5]:
        raise ValueError("Missing metadata. Please check out the example post https://gitlab.com/kubernetes-today/kubernetes-today-blog/-/raw/develop/posts/article.contributing.example.md")
    parts = text.strip().split('---', 2)
    metadata = {}
    content_part = ""
    if len(parts) > 2:
        metadata_part = parts[1].strip().split('\n')
        content_part = markdown(parts[2], extensions=['codehilite', 'fenced_code'], output_format='html5')
        content_part = nl2br(content_part)
        content_part = content_part.replace('</ul><br/>', '</ul>')
        content_part = content_part.replace('</ul><br>', '</ul>')
        content_part = content_part.replace('</code></pre></div><br/>', '</code></pre></div>')
        content_part = content_part.replace('</code></pre></div><br>', '</code></pre></div>')
        content_part = add_copy_buttons(content_part)
        for line in metadata_part:
            if ': ' in line:
                key, value = line.split(': ', 1)
                if key.strip() == 'tags':
                    metadata[key.strip()] = [tag.strip() for tag in value.split(',')]
                else:
                    metadata[key.strip()] = value.strip()
    return metadata, content_part

def render_and_write(template_name, context, output_path):
    """Render a template with the given context and write it to the specified output path."""
    template = env.get_template(f'{template_name}.html')
    content = template.render(context)
    with open(output_path, 'w', encoding='utf-8') as file:
        file.write(content)
    print(f"Generated {output_path}")

def categorize_posts_by_tags(posts):
    """Organize posts by their tags."""
    tags = {}
    for post in posts:
        metadata = post.get('metadata', {})
        for tag in metadata.get('tags', []):
            tags.setdefault(tag, []).append({
                "title": metadata.get('title', 'Untitled'),
                "author": metadata.get('author', 'Untitled'),
                "date": metadata.get('date', 'Untitled'),
                "filename": post.get('filename', 'NoFilename')
            })
    return tags

def generate_static_pages(pages):
    """Generate static HTML pages for predefined page names."""
    for page in pages:
        output_path = os.path.join(OUTPUT_DIR, f'{page}.html')
        render_and_write(page, {}, output_path)

def generate_tag_pages(tags):
    """Generate HTML pages for each tag, including a list of related articles."""
    for tag, articles in tags.items():
        output_file = tag.lower().replace(' ', '-') + '.html'
        output_path = os.path.join(OUTPUT_DIR, output_file)
        context = {
            'tag': tag,
            'articles': articles,
            'tags': tags.keys()
        }
        render_and_write('tag', context, output_path)

def generate_article_pages(posts, tags):
    """Generate HTML pages for each article."""
    for post in posts:
        metadata = post['metadata']
        output_path = os.path.join(OUTPUT_DIR, post['filename'].replace('.md', '.html'))
        context = {
            'title': metadata['title'],
            'date': metadata['date'],
            'content': post['content'],
            'tags': tags.keys(),
            'author': metadata['author']
        }
        render_and_write('article', context, output_path)

def calculate_font_sizes(tags):
    """Calculate font sizes for tags based on the number of articles."""
    min_font_size = 20
    max_font_size = 48
    return {tag: max(min_font_size, min(int(min_font_size + (math.log(len(articles)) * 5)), max_font_size))
            for tag, articles in tags.items()}

def generate_html(posts):
    """Generate HTML pages from posts, handling tags, articles, and static pages efficiently."""
    tags = categorize_posts_by_tags(posts)
    generate_static_pages(['index', 'about'])
    tag_context = {'tags_and_sizes': calculate_font_sizes(tags)}
    render_and_write('tags', tag_context, os.path.join(OUTPUT_DIR, 'tags.html'))
    generate_article_pages(posts, tags)
    generate_tag_pages(tags)

def copy_static_files():
    """Copy CSS and images to the output folder."""
    for subdir in ['css', 'images']:
        source = os.path.join(STATIC_DIR, subdir)
        destination = os.path.join(OUTPUT_DIR, subdir)
        if not os.path.exists(destination):
            os.makedirs(destination)
        for filename in os.listdir(source):
            src_file = os.path.join(source, filename)
            dst_file = os.path.join(destination, filename)
            shutil.copy(src_file, dst_file)
            print(f"Copied {filename} to {dst_file}")

if __name__ == "__main__":
    posts = load_posts()
    generate_html(posts)
    copy_static_files()