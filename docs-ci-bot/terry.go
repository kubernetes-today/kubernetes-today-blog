package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	messages := []string{
		"Hey there! Looks like Link snagged all my arrows again for his big showdown with Ganon. All I have got left is this magical link—not the pointy kind, but it might just save the day. Handle with care!",
		"Greetings! Sadly, my inventory is as empty as a dungeon after Link has been through. But fear not! I have still got one precious item—a link, not the triforce kind, but pretty legendary. Use it wisely!",
		"Hello, brave soul! Link is off battling evil, and he has taken every last arrow of mine. But here is something even Ganon cant dodge—a handy link to something useful. Guard it well!",
		"Would you believe it? I had a full stock of potions until a hero in green showed up. Now, all I can offer is advice and this rather interesting anecdote about my last encounter with a cucco!",
		"If you are looking for shields, I am all out. A mischievous korok passed by and traded them for some magic beans. They havent sprouted yet, but when they do, I hope they are worth the hype!",
		"Bargain of the day: Absolutely nothing! Yes, you heard right. It seems our favorite green-clad hero has been here before you and left the shelves bare. But come back tomorrow; I hear there is a shipment of mystery items coming in!",
	}
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	randomIdx := r.Intn(len(messages))
	selectedMessage := messages[randomIdx]

	fmt.Println(selectedMessage)
}
