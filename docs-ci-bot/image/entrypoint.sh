#!/bin/bash

git clone --branch develop https://gitlab.com/kubernetes-today/kubernetes-today-blog.git
cd kubernetes-today-blog
python3 -m pip install -r requirements.txt
python3 generate.py
python3 -m http.server --directory public 8080
