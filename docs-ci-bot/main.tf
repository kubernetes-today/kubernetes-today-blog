resource "google_cloud_run_v2_service" "default" {
  name     = "cloudrun-service"
  location = "us-central1"
  ingress  = "INGRESS_TRAFFIC_ALL"
  project  = "free-cloud-425414"

  template {
    containers {
      image = "acarnesecchi/kubernetes-today-cloud-ft:1.1"
      env {
        name  = "BRANCH"
        value = "develop"
      }
      resources {
        limits = {
          cpu    = "1"
          memory = "512Mi"
        }
      }
    }
    scaling {
      max_instance_count = "1"
    }
  }
}

resource "google_cloud_run_service_iam_binding" "default" {
  project  = google_cloud_run_v2_service.default.project
  location = google_cloud_run_v2_service.default.location
  service  = google_cloud_run_v2_service.default.name
  role     = "roles/run.invoker"
  members = [
    "allUsers"
  ]
}

